package utils

import (
	"bytes"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"time"

	"golang.org/x/crypto/bcrypt"
)

// MakePidFile create the file of process ID.
func MakePidFile(pidfile string) error {
	if pidfile == "" {
		return errors.New("*** pidfile not configured.")
	}

	var err error

	if err = os.MkdirAll(filepath.Dir(pidfile), os.FileMode(0755)); err != nil {
		return fmt.Errorf("*** fail to make the directory %v: %v", filepath.Dir(pidfile), err)
	}

	file, err := os.OpenFile(pidfile, os.O_WRONLY|os.O_CREATE, 0644)
	if err != nil {
		return fmt.Errorf("*** error opening pidfile %s: %s", pidfile, err)
	}
	defer file.Close()

	_, err = fmt.Fprintf(file, "%d", os.Getpid())
	if err != nil {
		return fmt.Errorf("*** fail to write the pid to %v: %v", pidfile, err)
	}

	return nil
}

// PostAsJSON request the JSON by POST method.
func PostAsJSON(url, apikey string, jsonStruct interface{}) (int, string, error) {
	ePrefix := "util::PostAsJSON:"

	jsonStr, jerr := json.Marshal(jsonStruct)
	if jerr != nil {
		msg := fmt.Sprintf("%v on stringify JSON: %v", ePrefix, jerr)
		return 500, msg, fmt.Errorf(msg)
	}
	req, qerr := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	if qerr != nil {
		msg := fmt.Sprintf("%v on create request: %v", ePrefix, qerr)
		return 500, msg, fmt.Errorf(msg)
	}

	req.Header.Set("Apikey", apikey)
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	res, serr := client.Do(req)
	if serr != nil {
		msg := fmt.Sprintf("%v on request: %v", ePrefix, serr)
		return 500, msg, fmt.Errorf(msg)
	}
	defer res.Body.Close()

	body, _ := ioutil.ReadAll(res.Body)

	return res.StatusCode, string(body), nil
}

// func CreateHash() string {
// 	t := time.Now().UnixNano()
// 	hash := sha256.Sum256([]byte(strconv.FormatInt(t, 10)))

// 	return hex.EncodeToString(hash[:])
// }

// CreateRandomHash create the random hash by SHA256.
func CreateRandomHash(salt string) string {
	t := time.Now().UnixNano()
	hash := sha256.Sum256([]byte(salt + strconv.FormatInt(t, 10)))

	return hex.EncodeToString(hash[:])
}

// CreatePasswordHash create the
func CreatePasswordHash(p string) (string, error) {
	// Use GenerateFromPassword to hash & salt pwd.
	// MinCost is just an integer constant provided by the bcrypt
	// package along with DefaultCost & MaxCost.
	// The cost can be any value you want provided it isn't lower
	// than the MinCost (4)
	hash, err := bcrypt.GenerateFromPassword([]byte(p), bcrypt.MinCost)
	if err != nil {
		return "", err
	}

	return string(hash), nil
}

// ComparePasswords compare the given the hash and original password and return result by boolean.
func ComparePasswords(hashedPwd string, plainPwd string) bool {
	// Since we'll be getting the hashed password from the DB it
	// will be a string so we'll need to convert it to a byte slice
	err := bcrypt.CompareHashAndPassword([]byte(hashedPwd), []byte(plainPwd))
	if err != nil {
		log.Println(err)
		return false
	}

	return true
}

// GetLocalIP returns the non loopback local IP of the host
func GetLocalIP() string {
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		return ""
	}
	for _, address := range addrs {
		// check the address type and if it is not a loopback the display it
		if ipnet, ok := address.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				return ipnet.IP.String()
			}
		}
	}
	return ""
}
